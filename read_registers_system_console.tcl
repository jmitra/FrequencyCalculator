# Written By Jubin Mitra
# Dated: 17 - 02 - 2016

set master_service_path [lindex [get_service_paths master] 0]
open_service master $master_service_path
master_read_32 $master_service_path 0x10 0x2

proc hex2dec {largeHex} {
    set res 0
    foreach hexDigit [split $largeHex {}] {
        set new 0x$hexDigit
        set res [expr {16*$res + $new}]
    }
    return $res
}

set freq [lindex [master_read_32 $master_service_path 0x10 0x2] 0]
set freq_num [format "%0x" $freq]
hex2dec $freq_num

