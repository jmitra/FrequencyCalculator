-------------------------------------------------------------------------------
-- Title      : INTEGER N FREQUENCY DIVIDER
-- Project    : CLOCK GENERATOR AND PHASE CALC 
-------------------------------------------------------------------------------
-- File       : FrequencyCalculator_tb.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 15-01-2015
-- Last update: 15-01-2015
-- Platform   : 
-- Standard   : VHDL'93/08
-------------------------------------------------------------------------------
-- Description: 
-- Test Bench for FREQUENCY DIVIDER								
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 15-01-2015  1.0      Jubin	Created
-------------------------------------------------------------------------------




--=================================================================================================--
--#################################################################################################--
--=================================================================================================--


-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
USE ieee.std_logic_arith.all; 
USE ieee.STD_LOGIC_UNSIGNED.all; 
use ieee.numeric_std.all;


--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity FrequencyCalculator_tb is

end FrequencyCalculator_tb;

--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture behavioral of FrequencyCalculator_tb is
 
--=================================================================================================--
						--========####  Component Declaration  ####========-- 
--=================================================================================================--   

component FrequencyCalculator is
   generic (
		constant REFERENCE_FREQUENCY_VALUE       : integer:= 8
   );
   port (
      RESET_I                                    : in  std_logic;
      CLK_REF_I		                             : in  std_logic; 
      CLK_UNKNOWN_I	                             : in  std_logic; 
      CLK_FREQUENCY_VALUE_O 					 : out std_logic_vector(31 downto 0);
      COMPUTATION_DONE_O                         : out std_logic
   );
end component;

	

--=================================================================================================--
						--========####  Signal Declaration  ####========-- 
--=================================================================================================--   
signal RESET									: std_logic := '0';
signal REF_CLK									: std_logic := '0';
signal UNKNOWN_CLK								: std_logic := '0';
signal CLK_FREQUENCY_VALUE 					    : std_logic_vector(31 downto 0);
signal COMPUTATION_DONE                         : std_logic := '0';

constant refclk_period	 						: time := 20   ns;			-- 50 MHz
constant unknownclk_period	 			    	: time := 24.95 ns;			-- 40.079 MHz
constant wait_period		 					: time := 10    ns;			

--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--  
   
   --==================================== Port Mapping ======================================--

   FrequencyCalculator_comp: 
   FrequencyCalculator
      generic map(
        REFERENCE_FREQUENCY_VALUE               => 50
      
      port map(
      RESET_I                                   => RESET,
      CLK_REF_I   	                            => REF_CLK,
      CLK_UNKNOWN_I   	                        => UNKNOWN_CLK,
      CLK_FREQUENCY_VALUE_O                     => CLK_FREQUENCY_VALUE,
      COMPUTATION_DONE_O                        => COMPUTATION_DONE
   );
   --==================================== Clock Generation =====================================--

   refclk_gen: process
   begin
		REF_CLK <= '1';
		wait for refclk_period/2;
		REF_CLK <= '0';
		wait for refclk_period/2;
   end process;
   
   unkownclk_gen: process
   begin
		UNKNOWN_CLK <= '1';
		wait for unknownclk_period/2;
		UNKNOWN_CLK <= '0';
		wait for unknownclk_period/2;
   end process;

   --==================================== User Logic =====================================--
   
	test_proc:
	process
	begin
		RESET <= '1';
		wait for wait_period*10;
		RESET <= '0';
		wait for wait_period*10;
		wait;

	end process;
		
   --=====================================================================================--     
end behavioral;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--